package store

import (
	"crypto/sha1"
	"errors"
	"fmt"

	"github.com/hashicorp/go-memdb"
	"github.com/vx-labs/iot-mqtt-router/tree"
	"github.com/vx-labs/iot-mqtt-router/types"
)

type memDBStore struct {
	db           *memdb.MemDB
	patternIndex *topicIndexer
}

func NewMemDBStore() (*memDBStore, error) {
	db, err := memdb.NewMemDB(&memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"subscriptions": &memdb.TableSchema{
				Name: "subscriptions",
				Indexes: map[string]*memdb.IndexSchema{
					"id": &memdb.IndexSchema{
						Name:         "id",
						AllowMissing: false,
						Unique:       true,
						Indexer: &memdb.StringFieldIndex{
							Field: "Id",
						},
					},
					"tenant": &memdb.IndexSchema{
						Name:         "tenant",
						AllowMissing: false,
						Unique:       false,
						Indexer:      &memdb.StringFieldIndex{Field: "Tenant"},
					},
					"session": &memdb.IndexSchema{
						Name:         "session",
						AllowMissing: false,
						Unique:       false,
						Indexer:      &memdb.StringFieldIndex{Field: "Session"},
					},
				},
			},
		},
	})
	if err != nil {
		return nil, err
	}
	return &memDBStore{
		db:           db,
		patternIndex: TenantTopicIndexer(),
	}, nil
}

type topicIndexer struct {
	root *tree.INode
}

func TenantTopicIndexer() *topicIndexer {
	return &topicIndexer{
		root: tree.NewINode(),
	}
}
func (t *topicIndexer) Remove(tenant, id string, pattern []byte) error {
	return t.root.Remove(tenant, id, types.Topic(pattern))
}
func (t *topicIndexer) Lookup(tenant string, pattern []byte) (types.SubscriptionList, error) {
	set := t.root.Select(tenant, nil, types.Topic(pattern))
	return set, nil
}

func (s *topicIndexer) Index(subscription *types.Subscription) error {
	s.root.Insert(
		types.Topic(subscription.Pattern),
		subscription.Tenant,
		subscription,
	)
	return nil
}
func (m *memDBStore) do(write bool, f func(*memdb.Txn) error) error {
	tx := m.db.Txn(write)
	defer tx.Abort()
	return f(tx)
}
func (m *memDBStore) read(f func(*memdb.Txn) error) error {
	return m.do(false, f)
}
func (m *memDBStore) write(f func(*memdb.Txn) error) error {
	return m.do(true, f)
}

func (m *memDBStore) first(tx *memdb.Txn, index string, value ...interface{}) (*types.Subscription, error) {
	var ok bool
	var res *types.Subscription
	data, err := tx.First("subscriptions", index, value...)
	if err != nil {
		return res, err
	}
	res, ok = data.(*types.Subscription)
	if !ok {
		return res, errors.New("invalid type fetched")
	}
	return res, nil
}
func (m *memDBStore) all(tx *memdb.Txn, index string, value ...interface{}) (types.SubscriptionList, error) {
	var set types.SubscriptionList
	iterator, err := tx.Get("subscriptions", index, value...)
	if err != nil {
		return set, err
	}
	for {
		data := iterator.Next()
		if data == nil {
			return set, nil
		}
		res, ok := data.(*types.Subscription)
		if !ok {
			return set, errors.New("invalid type fetched")
		}
		set = append(set, res)
	}
}

func (m *memDBStore) ByID(id string) (*types.Subscription, error) {
	var res *types.Subscription
	return res, m.read(func(tx *memdb.Txn) (err error) {
		res, err = m.first(tx, "id", id)
		return
	})
}
func (m *memDBStore) ByTenant(tenant string) (types.SubscriptionList, error) {
	var res types.SubscriptionList
	return res, m.read(func(tx *memdb.Txn) (err error) {
		res, err = m.all(tx, "tenant", tenant)
		return
	})
}
func (m *memDBStore) BySession(session string) (types.SubscriptionList, error) {
	var res types.SubscriptionList
	return res, m.read(func(tx *memdb.Txn) (err error) {
		res, err = m.all(tx, "session", session)
		return
	})
}
func (m *memDBStore) ByTopic(tenant string, pattern []byte) (types.SubscriptionList, error) {
	return m.patternIndex.Lookup(tenant, pattern)
}
func (m *memDBStore) Sessions() ([]string, error) {
	var res types.SubscriptionList
	err := m.read(func(tx *memdb.Txn) (err error) {
		res, err = m.all(tx, "session")
		return
	})
	if err != nil {
		return nil, err
	}
	out := make([]string, len(res))
	for idx := range res {
		out[idx] = res[idx].Session
	}
	return out, nil
}
func (m *memDBStore) Delete(id string) error {
	session, err := m.ByID(id)
	if err != nil {
		return err
	}
	err = m.patternIndex.Remove(session.Tenant, session.Id, session.Pattern)
	if err != nil {
		return err
	}
	return m.write(func(tx *memdb.Txn) error {
		err = tx.Delete("subscriptions", session)
		if err != nil {
			return err
		}
		tx.Commit()
		return nil
	})
}
func MakeSubscriptionID(session string, pattern []byte) (string, error) {
	hash := sha1.New()
	_, err := hash.Write([]byte(session))
	if err != nil {
		return "", err
	}
	_, err = hash.Write(pattern)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", hash.Sum(nil)), nil
}

func (m *memDBStore) Create(message *types.Subscription) error {
	if message.Id == "" {
		id, err := MakeSubscriptionID(message.Session, message.Pattern)
		if err != nil {
			return err
		}
		message.Id = id
	}
	err := m.patternIndex.Index(message)
	if err != nil {
		return err
	}
	return m.write(func(tx *memdb.Txn) error {
		err := tx.Insert("subscriptions", message)
		if err != nil {
			return err
		}
		tx.Commit()
		return nil
	})
}
