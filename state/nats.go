package state

import (
	"errors"
	"log"

	"github.com/golang/protobuf/proto"
	"github.com/google/uuid"
	"github.com/nats-io/go-nats-streaming"
	"github.com/vx-labs/iot-mqtt-events"
	"github.com/vx-labs/iot-mqtt-router/store"
	"github.com/vx-labs/iot-mqtt-router/types"
	"golang.org/x/net/context"
	"google.golang.org/grpc/health/grpc_health_v1"
)

const natsTopic = "vx.iot.mqtt"

type natsProvider struct {
	nats         stan.Conn
	store        stateStore
	subscription stan.Subscription
}

func (p *natsProvider) Close() error {
	p.subscription.Unsubscribe()
	return p.nats.Close()
}

func (p *natsProvider) Apply(event *events.MQTTEvent) error {
	switch event.Name {
	case "session_deleted":
		data := event.GetSessionDeleted()
		if data == nil {
			return errors.New("malformed session deleted event")
		}
		set, err := p.store.BySession(data.ID)
		if err != nil {
			return err
		}
		return set.ApplyE(func(subscription *types.Subscription) error {
			return p.store.Delete(subscription.Id)
		})
	case "session_unsubscribed":
		subscriptions := event.GetSessionUnsubscribed()
		if subscriptions == nil {
			return errors.New("malformed session unsubscribed event")
		}
		id, err := store.MakeSubscriptionID(
			subscriptions.ID, subscriptions.Topic,
		)
		if err != nil {
			return err
		}
		if err := p.store.Delete(id); err != nil {
			return err
		}

	case "session_subscribed":
		subscriptions := event.GetSessionSubscribed()
		if subscriptions == nil {
			return errors.New("malformed session subscribed event")
		}
		return p.store.Create(&types.Subscription{
			Pattern: subscriptions.Pattern,
			Qos:     subscriptions.Qos,
			Session: subscriptions.ID,
			Tenant:  subscriptions.Tenant,
		})
	}
	return nil
}

func NewNATSProvider(host, cluster string) *natsProvider {
	db, err := store.NewMemDBStore()
	if err != nil {
		panic(err)
	}
	p := &natsProvider{
		store: db,
	}
	clientID := uuid.New().String()
	sc, err := stan.Connect(cluster, clientID,
		stan.NatsURL(host),
		stan.Pings(5, 3))
	if err != nil {
		panic(err)
	}
	sub, err := sc.Subscribe(natsTopic, func(m *stan.Msg) {
		ev := &events.MQTTEvent{}
		err := proto.Unmarshal(m.Data, ev)
		if err != nil {
			log.Printf("ERR: failed to decode received event: %v", err)
			return
		}
		err = p.Apply(ev)
		if err != nil {
			log.Printf("WARN: failed to apply %s event: %v", ev.Name, err)
		}
	}, stan.StartAtSequence(0))
	if err != nil {
		panic(err)
	}
	p.nats = sc
	p.subscription = sub
	return p
}

func (e *natsProvider) ByTopic(tenant string, topic []byte) types.SubscriptionList {
	set, err := e.store.ByTopic(tenant, topic)
	if err != nil {
		return nil
	}
	return set
}
func (e *natsProvider) Unsubscribe(ctx context.Context, tenant, session string, pattern []byte) error {
	sessionUnsubscribed := events.SessionUnsubscribed{
		ID:     session,
		Tenant: tenant,
		Topic:  pattern,
	}
	ev := events.MQTTEvent{
		Name: "session_unsubscribed",
		Event: &events.MQTTEvent_SessionUnsubscribed{
			SessionUnsubscribed: &sessionUnsubscribed,
		},
	}
	payload, err := proto.Marshal(&ev)
	if err != nil {
		return err
	}
	return e.nats.Publish(natsTopic, payload)
}

func (e *natsProvider) Subscribe(ctx context.Context, req *types.SubscribeRequest) error {
	if req == nil {
		return errors.New("empty subscribe request provided")
	}
	if req.Session == "" {
		return errors.New("empty session ID provided")
	}
	if len(req.Pattern) == 0 {
		return errors.New("missing topic pattern")
	}
	if req.Qos > 2 {
		return errors.New("invalid QoS provided")
	}
	if req.Tenant == "" {
		return errors.New("missing tenant")
	}

	ev := events.MQTTEvent{
		Name: "session_subscribed",
		Event: &events.MQTTEvent_SessionSubscribed{
			SessionSubscribed: &events.SessionSubscribed{
				ID:      req.Session,
				Tenant:  req.Tenant,
				Pattern: req.Pattern,
				Qos:     req.Qos,
			},
		},
	}

	payload, err := proto.Marshal(&ev)
	if err != nil {
		return err
	}
	return e.nats.Publish(natsTopic, payload)
}
func (e *natsProvider) Sessions() ([]string, error) {
	return e.store.Sessions()
}

func (e *natsProvider) Status() grpc_health_v1.HealthCheckResponse_ServingStatus {
	return grpc_health_v1.HealthCheckResponse_SERVING
}
