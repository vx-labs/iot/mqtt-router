package types

//go:generate protoc -I${GOPATH}/src -I${GOPATH}/src/github.com/vx-labs/iot-mqtt-router/types/ --go_out=plugins=grpc:. types.proto

type NodeSubscription struct {
	Id        string
	SessionId string
	Topic     string
	Qos       byte
}

type TopicSessionSubscription struct {
	Session string
	Topic   string
	Qos     byte
}

type SubscriptionList []*Subscription

type SubscriptionFilter func(*Subscription) bool

func HasTenant(tenant string) SubscriptionFilter {
	return func(s *Subscription) bool {
		return s.Tenant == tenant
	}
}

func HasSessionID(id string) SubscriptionFilter {
	return func(s *Subscription) bool {
		return s.Session == id
	}
}

func (set SubscriptionList) Filter(filters ...SubscriptionFilter) SubscriptionList {
	copy := make(SubscriptionList, 0, cap(set))
	for _, sub := range set {
		matched := true
		for _, f := range filters {
			if !f(sub) {
				matched = false
			}
		}
		if matched {
			copy = append(copy, sub)
		}
	}
	return copy
}

func (set SubscriptionList) Apply(f func(*Subscription)) {
	for _, sub := range set {
		f(sub)
	}
}
func (set SubscriptionList) ApplyE(f func(*Subscription) error) (err error) {
	for _, sub := range set {
		err = f(sub)
		if err != nil {
			break
		}
	}
	return err
}
