package api

import (
	"context"
	"io"

	"github.com/vx-labs/iot-mqtt-router/types"
	"google.golang.org/grpc"
)

type Client struct {
	conn io.Closer
	api  types.RouterServiceClient
}

func New(conn *grpc.ClientConn) *Client {
	return &Client{
		api: types.NewRouterServiceClient(conn),
	}
}

type SubscriptionFilter func(s *types.SubscriptionSelector)

func HasTenant(tenant string) SubscriptionFilter {
	return func(s *types.SubscriptionSelector) {
		s.Tenant = tenant
	}
}
func MatchesPattern(pattern []byte) SubscriptionFilter {
	return func(s *types.SubscriptionSelector) {
		s.Topic = pattern
	}
}

func (c *Client) List(ctx context.Context, filters ...SubscriptionFilter) (types.SubscriptionList, error) {
	selector := &types.SubscriptionSelector{
		Tenant: "_default",
	}
	for _, f := range filters {
		f(selector)
	}
	elements, err := c.api.List(ctx, selector)
	if err != nil {
		return nil, err
	}
	return types.SubscriptionList(elements.Subscriptions), nil
}

func (c *Client) Subscribe(ctx context.Context, tenant, session string, pattern []byte, qos int32) error {

	r := &types.SubscribeRequest{
		Session: session,
		Tenant:  tenant,
		Qos:     qos,
		Pattern: pattern,
	}
	_, err := c.api.Subscribe(ctx, r)
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) Unsubscribe(ctx context.Context, tenant, session string, topic []byte) error {
	r := &types.UnsubscribeRequest{
		Pattern: topic,
		Tenant:  tenant,
		Session: session,
	}
	_, err := c.api.Unsubscribe(ctx, r)
	return err
}
