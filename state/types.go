package state

import (
	"github.com/vx-labs/iot-mqtt-router/types"
)

type stateStore interface {
	ByTopic(tenant string, pattern []byte) (types.SubscriptionList, error)
	ByID(id string) (*types.Subscription, error)
	BySession(id string) (types.SubscriptionList, error)
	Sessions() ([]string, error)
	Create(subscription *types.Subscription) error
	Delete(id string) error
}
