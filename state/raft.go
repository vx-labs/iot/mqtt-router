package state

import (
	"errors"
	"fmt"
	"io"
	"log"

	"github.com/google/uuid"
	"google.golang.org/grpc/health/grpc_health_v1"

	"github.com/vx-labs/iot-mqtt-router/store"

	"github.com/gogo/protobuf/proto"
	"github.com/vx-labs/iot-mqtt-router/types"
	state "github.com/vx-labs/state-store"
	"golang.org/x/net/context"
)

type raftProvider struct {
	store stateStore
	state state.Store
}

const (
	topicSubscribed = iota
	topicUnsubscribed
)

func NewRaftProvider(serviceName, clusterServiceName, serfPort, raftPort string, expectedPeers int) *raftProvider {
	db, err := store.NewMemDBStore()
	if err != nil {
		panic(err)
	}
	p := &raftProvider{
		store: db,
	}
	stateStore, err := state.NewStore(p,
		state.WithExpectedPeers(expectedPeers),
		state.WithSerfPortName(serfPort),
		state.WithRaftPortName(raftPort),
		state.WithServiceName(clusterServiceName))
	if err != nil {
		panic(err)
	}
	p.state = stateStore
	go p.state.RegisterStatefulService(serviceName)
	return p
}

func (e *raftProvider) Restore(io.ReadCloser) error {
	return errors.New("not implemented")
}
func (e *raftProvider) Snapshot() (io.ReadCloser, error) {
	return nil, errors.New("not implemented")
}
func (e *raftProvider) Apply(payload []byte) error {
	var action func() error
	switch payload[0] {
	case topicSubscribed:
		ev := &types.SubscribeRequest{}
		err := proto.Unmarshal(payload[1:], ev)
		if err != nil {
			return err
		}
		action = func() error {
			sub := &types.Subscription{
				Id:      uuid.New().String(),
				Pattern: []byte(ev.Pattern),
				Qos:     ev.Qos,
				Session: ev.Session,
				Tenant:  ev.Tenant,
			}
			return e.store.Create(sub)
		}
	case topicUnsubscribed:
		ev := &types.UnsubscribeRequest{}
		err := proto.Unmarshal(payload[1:], ev)
		if err != nil {
			return err
		}
		action = func() error {
			set, err := e.store.ByTopic(ev.Tenant, []byte(ev.Pattern))
			if err != nil {
				return err
			}
			set = set.Filter(types.HasSessionID(ev.Session))
			return set.ApplyE(func(s *types.Subscription) error {
				err := e.store.Delete(s.Id)
				if err != nil {
					log.Printf("WARN: failed to delete subscription %s: %v", s.Id, err)
				}
				return nil
			})
		}
	default:
		log.Println("unknown event received")
		return fmt.Errorf("unknown event received")
	}
	return action()
}

func (e *raftProvider) ByTopic(tenant string, topic []byte) types.SubscriptionList {
	set, err := e.store.ByTopic(tenant, topic)
	if err != nil {
		return nil
	}
	return set
}
func (e *raftProvider) Unsubscribe(ctx context.Context, tenant, session string, pattern []byte) error {
	message, err := proto.Marshal(&types.UnsubscribeRequest{
		Session: session,
		Tenant:  tenant,
		Pattern: pattern,
	})
	payload := make([]byte, len(message)+1)
	payload[0] = topicUnsubscribed
	copy(payload[1:], message)
	if err != nil {
		return err
	}
	return e.state.Apply(payload)
}

func (e *raftProvider) Subscribe(ctx context.Context, req *types.SubscribeRequest) error {
	message, err := proto.Marshal(req)
	payload := make([]byte, len(message)+1)
	payload[0] = topicSubscribed
	copy(payload[1:], message)
	if err != nil {
		return err
	}
	return e.state.Apply(payload)
}
func (e *raftProvider) Sessions() ([]string, error) {
	return e.store.Sessions()
}
func (e *raftProvider) RemoveSession(ctx context.Context, session string) error {
	set, err := e.store.BySession(session)
	if err != nil {
		return err
	}
	return set.ApplyE(func(n *types.Subscription) error {
		return e.Unsubscribe(ctx, n.Tenant, session, n.Pattern)
	})
}
func (e *raftProvider) Leader() bool {
	ok, _ := e.state.Leader()
	return ok
}

func (e *raftProvider) Status() grpc_health_v1.HealthCheckResponse_ServingStatus {
	status := e.state.Status()
	if status.Member {
		return grpc_health_v1.HealthCheckResponse_SERVING
	} else {
		return grpc_health_v1.HealthCheckResponse_NOT_SERVING
	}
}
func (e *raftProvider) Topology() *state.ReadTopologyReply {
	return e.state.Topology()
}
