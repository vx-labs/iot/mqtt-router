package commands

import (
	"context"
	"fmt"
	"log"
	"text/template"

	"github.com/manifoldco/promptui"

	"github.com/vx-labs/iot-mqtt-router/types"

	"github.com/spf13/cobra"
	"github.com/vx-labs/iot-mqtt-router/api"
	"google.golang.org/grpc"
)

func ToString(b []byte) string {
	return string(b)
}

const subscriptionTemplate = `• {{ .Id | green | bold }}
  {{ "Session:"       | faint }} {{ .Session }}
  {{ "Topic pattern:"       | faint }} {{ .Pattern | toString }}
  {{ "QoS:"       | faint }} {{ .Qos }}`

type EndpointProvider interface {
	Endpoint(service string) (endpoint string, token string)
}

func Subscriptions(ctx context.Context, helper EndpointProvider) *cobra.Command {
	cmd := &cobra.Command{
		Use:     "subscriptions",
		Aliases: []string{"sub"},
		Short:   "manage mqtt subscriptions",
	}
	cmd.AddCommand(list(ctx, helper))
	cmd.AddCommand(create(ctx, helper))
	cmd.AddCommand(delete(ctx, helper))
	return cmd
}
func getClient(helper EndpointProvider) *api.Client {
	endpoint, _ := helper.Endpoint("subscriptions")
	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	client := api.New(conn)
	return client
}
func delete(ctx context.Context, helper EndpointProvider) *cobra.Command {
	c := &cobra.Command{
		Use:     "delete",
		Aliases: []string{"rm"},
		Args:    cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, topics []string) {
			client := getClient(helper)
			tenant, err := cmd.Flags().GetString("tenant")
			if err != nil {
				log.Fatalln(err)
			}
			session, err := cmd.Flags().GetString("session")
			if err != nil {
				log.Fatalln(err)
			}
			for _, topic := range topics {
				err := client.Unsubscribe(ctx, tenant, session, []byte(topic))
				if err != nil {
					log.Printf("WARN: failed to unsubscribe session %s: %v", session, err)
				} else {
					log.Println(topic)
				}
			}
		},
	}
	c.Flags().StringP("tenant", "n", "_default", "use given tenant")
	c.Flags().StringP("session", "i", "", "unsubscribe the given session")
	return c

}
func create(ctx context.Context, helper EndpointProvider) *cobra.Command {
	c := &cobra.Command{
		Use:  "create",
		Args: cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, topics []string) {
			client := getClient(helper)
			qos, err := cmd.Flags().GetInt32("qos")
			if err != nil {
				log.Fatalln(err)
			}
			tenant, err := cmd.Flags().GetString("tenant")
			if err != nil {
				log.Fatalln(err)
			}
			session, err := cmd.Flags().GetString("session")
			if err != nil {
				log.Fatalln(err)
			}
			for _, topic := range topics {
				err := client.Subscribe(ctx, tenant, session, []byte(topic), qos)
				if err != nil {
					log.Printf("WARN: failed to subscribe session %s: %v", session, err)
				} else {
					log.Println(topic)
				}
			}
		},
	}
	c.Flags().StringP("tenant", "n", "_default", "use given tenant")
	c.Flags().Int32P("qos", "q", 0, "use given qos")
	c.Flags().StringP("session", "i", "", "subscribe the given session")
	return c
}
func list(ctx context.Context, helper EndpointProvider) *cobra.Command {
	c := &cobra.Command{
		Use: "list",
		Run: func(cmd *cobra.Command, _ []string) {
			client := getClient(helper)

			parsed, err := template.New(
				"",
			).Funcs(promptui.FuncMap).Funcs(
				template.FuncMap{
					"toString": ToString,
				},
			).Parse(fmt.Sprintf("%s\n", subscriptionTemplate))
			if err != nil {
				log.Fatal(err)
			}

			tenant, err := cmd.Flags().GetString("tenant")
			if err != nil {
				log.Fatal(err)
			}
			topic, err := cmd.Flags().GetString("pattern")
			if err != nil {
				log.Fatal(err)
			}

			opts := []api.SubscriptionFilter{
				api.HasTenant(tenant),
				api.MatchesPattern([]byte(topic)),
			}
			set, err := client.List(ctx, opts...)
			if err != nil {
				log.Fatal(err)
			}
			err = set.ApplyE(func(s *types.Subscription) error {
				return parsed.Execute(cmd.OutOrStdout(), s)
			})
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	c.Flags().StringP("pattern", "t", "", "filter subscription using the given topic pattern")
	c.Flags().StringP("tenant", "n", "_default", "use given tenant")
	return c
}
