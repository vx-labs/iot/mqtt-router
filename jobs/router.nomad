job "subscriptions" {
  datacenters = ["dc1"]
  type        = "service"

  constraint {
    operator = "distinct_hosts"
    value    = "true"
  }

  update {
    max_parallel     = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    health_check     = "checks"
    auto_revert      = true
    canary           = 0
  }

  group "app" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "15s"
      mode     = "delay"
    }

    ephemeral_disk {
      size = 300
    }

    task "store" {
      driver = "docker"

      env {
        CONSUL_HTTP_ADDR = "172.17.0.1:8500"
        SESSIONS_HOST    = "172.17.0.1:4141"
        BACKEND          = "nats"
        NATS_URL         = "nats://servers.nats.discovery.par1.vx-labs.net:4222"
      }

      config {
        force_pull = true
        image      = "quay.io/vxlabs/iot-mqtt-router:v4.0.0"

        port_map {
          RouterService = 7997
          health        = 9000
        }
      }

      resources {
        cpu    = 100
        memory = 128

        network {
          mbits = 10
          port  "RouterService"{}
          port  "health"{}
        }
      }

      service {
        name = "RouterService"
        port = "RouterService"
        tags = ["leader"]

        check {
          type     = "http"
          path     = "/health"
          port     = "health"
          interval = "5s"
          timeout  = "2s"
        }
      }
    }
  }
}
