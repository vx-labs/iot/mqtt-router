package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"

	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"

	sessions "github.com/vx-labs/iot-mqtt-sessions/api"

	"google.golang.org/grpc/health/grpc_health_v1"

	"github.com/sirupsen/logrus"
	"github.com/vx-labs/iot-mqtt-router/state"
	"github.com/vx-labs/iot-mqtt-router/tracing"
	"github.com/vx-labs/iot-mqtt-router/types"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type StateProvider interface {
	ByTopic(tenant string, topic []byte) types.SubscriptionList
	Unsubscribe(ctx context.Context, tenant, session string, pattern []byte) error
	Subscribe(ctx context.Context, req *types.SubscribeRequest) error
	Status() grpc_health_v1.HealthCheckResponse_ServingStatus
	Sessions() ([]string, error)
}

type Router struct {
	sessions *sessions.Client
	state    StateProvider
}

func main() {
	logger := logrus.New()
	if os.Getenv("API_ENABLE_PROFILING") == "true" {
		go func() {
			logger.Println(http.ListenAndServe(":8080", nil))
		}()
	}
	tracer := tracing.Instance()

	port := ":7997"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	}
	sessionsConn, err := grpc.Dial(os.Getenv("SESSIONS_HOST"),
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(
			otgrpc.OpenTracingClientInterceptor(tracer),
		),
		grpc.WithStreamInterceptor(
			otgrpc.OpenTracingStreamClientInterceptor(tracer),
		),
	)
	if err != nil {
		log.Fatal(err)
	}
	s := grpc.NewServer(
		grpc.UnaryInterceptor(
			otgrpc.OpenTracingServerInterceptor(tracer),
		),
		grpc.StreamInterceptor(
			otgrpc.OpenTracingStreamServerInterceptor(tracer),
		),
	)

	r := &Router{
		sessions: sessions.New(sessionsConn),
	}
	switch os.Getenv("BACKEND") {
	case "nats":
		r.state = state.NewNATSProvider(os.Getenv("NATS_URL"), "events")
	case "raft":
		r.state = state.NewRaftProvider("RouterService", "subscriptions-cluster", "serf", "raft", 3)
	default:
		panic("no valid backend were specified")
	}
	go r.ServeHTTPHealth()
	types.RegisterRouterServiceServer(s, r)
	logrus.Infof("serving router on %v", port)
	if err := s.Serve(lis); err != nil {
		logrus.Fatalf("failed to serve: %v", err)
	}
}

func (t *Router) ServeHTTPHealth() {
	mux := http.NewServeMux()
	mux.HandleFunc("/health", func(w http.ResponseWriter, _ *http.Request) {
		nodestatus := t.state.Status()
		switch nodestatus {
		case grpc_health_v1.HealthCheckResponse_NOT_SERVING:
			w.WriteHeader(http.StatusTooManyRequests)
		case grpc_health_v1.HealthCheckResponse_SERVING:
			w.WriteHeader(http.StatusOK)
		default:
			w.WriteHeader(http.StatusServiceUnavailable)
		}
	})
	log.Println(http.ListenAndServe("[::]:9000", mux))
}

func (r *Router) Subscribe(ctx context.Context, request *types.SubscribeRequest) (*types.SubscribeReply, error) {
	if request.Tenant == "" {
		request.Tenant = "_default"
	}
	if request.Qos > 2 || request.Qos < 0 {
		return nil, fmt.Errorf("invalid qos provided")
	}
	logrus.Infof("subscribing %s to %s", request.Session, request.Pattern)
	err := r.state.Subscribe(ctx, request)
	if err != nil {
		return nil, err
	}
	return &types.SubscribeReply{}, nil
}
func (r *Router) Unsubscribe(ctx context.Context, request *types.UnsubscribeRequest) (*types.UnsubscribeReply, error) {
	if request.Tenant == "" {
		request.Tenant = "_default"
	}
	err := r.state.Unsubscribe(ctx, request.Tenant, request.Session, request.Pattern)
	return &types.UnsubscribeReply{}, err
}

func (r *Router) List(ctx context.Context, in *types.SubscriptionSelector) (*types.SubscriptionListResponse, error) {
	rt := r.state.ByTopic(in.Tenant, in.Topic)
	hits := map[string]struct{}{}
	subs := rt.Filter(func(subscription *types.Subscription) bool {
		_, ok := hits[subscription.Session]
		if !ok {
			hits[subscription.Session] = struct{}{}
		}
		return !ok
	})
	return &types.SubscriptionListResponse{
		Subscriptions: subs,
	}, nil
}
func (t *Router) Check(ctx context.Context, in *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	nodestatus := t.state.Status()
	return &grpc_health_v1.HealthCheckResponse{Status: nodestatus}, nil
}
